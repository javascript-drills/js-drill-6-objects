function pairs(obj) {
    let pairArray = [];

    if(typeof(obj) == "object"){
        for( let index in obj ){
            pairArray.push([index, obj[index]]);
        }
        return pairArray;
    }
    return 0;
}

module.exports = pairs;