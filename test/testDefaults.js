const {testObject} = require("../datasetObject");
const defaults = require("../defaults.js");

const defaultProps = { surname: 'Obama', age: 55, hobby: 'Flying'}
const defaultResult = defaults(testObject, defaultProps);

if(defaultResult.length != 0){
    console.log(defaultResult);
}
else{
    console.log("Wrong data passed");
}
