const {testObject} = require("../datasetObject");
const mapObject = require("../mapObject.js");

function callbackFunc(key, value){
    if(key === 'name'){
        return "JOKER";
    }
    else if(key === 'age'){
        return  value *2;
    }
    else{
        return "Hell";
    }
}
const mapobjResult = mapObject(testObject, callbackFunc);


if(mapobjResult.length != 0){
    console.log(mapobjResult);
}