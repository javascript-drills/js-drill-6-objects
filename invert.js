function invert(obj) {
    const invertObject = {};

    if(typeof(obj) == "object"){
        for( let index in obj ){
            invertObject[obj[index]] = index;
        }
        return invertObject;
    }
    return 0;
}

module.exports = invert;