function defaults(obj, defaultProps) {    
    if(typeof(obj) == "object"){
        for(let index in defaultProps){
            if(obj[index] == undefined){
                obj[index]  = defaultProps[index];
            }
        }
        return obj;
    }
    return 0;
}
module.exports = defaults;