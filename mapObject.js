const key = require('./keys.js');
const value = require('./values.js');

function mapObject(obj, cbfunc) {
    const keyData = key(obj);
    const valueData = value(obj);
    
    if(typeof(obj) == 'object'){
        for( let index=0; index<keyData.length; index++ ){
            obj[keyData[index]] = cbfunc(keyData[index], valueData[index]);        
        }

        return obj;
    }
    else{
        console.log("Wrong data passed.");
    }
    return {};
}

module.exports = mapObject;